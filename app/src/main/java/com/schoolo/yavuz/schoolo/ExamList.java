package com.schoolo.yavuz.schoolo;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;
import java.util.regex.Pattern;

public class ExamList extends ArrayAdapter {

    private Activity context;
    private List<Exam> exams;
    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;
    private String userid;


    public ExamList(@NonNull Activity context, final List<Exam> exams, DatabaseReference databaseReference)
    {
        super(context,R.layout.exam_list,exams);
        this.context = context;
        this.exams = exams;
        this.databaseReference = databaseReference;
    }

    public View getView(int pos, View view, ViewGroup parent)
    {

        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.exam_list,null,true);

        TextView txtName = (TextView) listViewItem.findViewById(R.id.txtNameForExam);

        Button btnDelete = (Button)    listViewItem.findViewById(R.id.btnDeleteForExam);

        Button btnUpdate = (Button) listViewItem.findViewById(R.id.btnUpdateForExam);

        firebaseAuth = FirebaseAuth.getInstance();

        final FirebaseUser user = firebaseAuth.getCurrentUser();
        userid  = user.getUid();

        final Exam exam = exams.get(pos);

        txtName.setText(exam.getLesson_name()+" Sınavı");

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference.child(exam.getId()).removeValue();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showUpdateDialog(exam.getId(),exam.getUserid(),exam.getDate(),exam.getLesson_name(),exam.getDescription());

            }
        });

        listViewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowExamDetail(exam.getDate(),exam.getLesson_name(),exam.getDescription());
            }
        });


        return listViewItem;

    }

    private void showUpdateDialog(final String examId,String userId,String editText_Date, String editText_LessonName,String editText_Description){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this.context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View dialogView = inflater.inflate(R.layout.update_dialog_exam,null);

        dialogBuilder.setView(dialogView);

        final EditText editDate = (EditText) dialogView.findViewById(R.id.editTextDate);
        editDate.setText(editText_Date);

        final EditText editLessonName= (EditText) dialogView.findViewById(R.id.editTextLessonName);
        editLessonName.setText(editText_LessonName);

        final EditText editDescription= (EditText) dialogView.findViewById(R.id.editTextDescription);
        editDescription.setText(editText_Description);
        final Button   UpButton = (Button) dialogView.findViewById(R.id.UpButtonForExam);

        final AlertDialog alertDialog = dialogBuilder.create();

        alertDialog.show();

        UpButton.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {

                String edit_Date = editDate.getText().toString().trim();
                String edit_lessonName = editLessonName.getText().toString().trim();
                String edit_Description = editDescription.getText().toString().trim();

                if(TextUtils.isEmpty(edit_Date))
                {
                    editDate.setError("Bu alan doldurulmalıdır!");
                    return;
                }
                if(TextUtils.isEmpty(edit_lessonName))
                {
                    editLessonName.setError("Bu alan doldurulmalıdır!");
                    return;
                }
                if(TextUtils.isEmpty(edit_Description))
                {
                    editDescription.setError("Bu alan doldurulmalıdır!");
                    return;
                }
                String dateRegex = "\\d{2}/\\d{2}/\\d{4}";
                Pattern pat = Pattern.compile(dateRegex);
                if(pat.matcher(edit_Date).matches() == false)
                {
                    editDate.setError("Formatı gg/aa/yyyy gibi olmalıdır.");
                }else
                    {
                        updateExam(examId,userid,edit_lessonName,edit_Date,edit_Description);
                        alertDialog.dismiss();
                    }
            }
        });

    }
    private boolean updateExam(String examId,String userId,String editText_lesson,String editText_date,String editText_description)
    {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("exams").child(examId);

        Exam exam = new Exam(examId,userId,editText_lesson,editText_date,editText_description);

        databaseReference.setValue(exam);

        Toast.makeText(context,"Sınav bilgileri güncellendi!",Toast.LENGTH_LONG).show();

        return true;
    }

    private void ShowExamDetail(String editText_Date,String editText_Lesson,String editText_Description )
    {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this.context);

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View dialogView = layoutInflater.inflate(R.layout.exam_detaile,null);

        dialogBuilder.setView(dialogView);

        final TextView viewDate = (TextView) dialogView.findViewById(R.id.textView_DateExam);

        viewDate.setText(editText_Date);

        final TextView viewLesson = (TextView) dialogView.findViewById(R.id.textView_LessonExam);

        viewLesson.setText(editText_Lesson);

        final TextView viewDescription = (TextView) dialogView.findViewById(R.id.textView_DescriptionExam);

        viewDescription.setText(editText_Description);

        final AlertDialog alertDialog = dialogBuilder.create();

        alertDialog.show();

    }

}
