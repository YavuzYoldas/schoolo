package com.schoolo.yavuz.schoolo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class HomePageActivity extends AppCompatActivity {

    private Button btnHomeWork;
    private Button btnExam;
    private Button btnNote;
    private Button btnExit;
    private TextView TextViewuser;
    private FirebaseAuth firebaseAuth;
    private String userEmail;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        btnHomeWork = (Button) findViewById(R.id.button_HomeWork);
        btnExam = (Button) findViewById(R.id.button_Exam);
        btnNote = (Button) findViewById(R.id.button_Annoucement);
        btnExit = (Button) findViewById(R.id.buttonExit);
        TextViewuser = (TextView) findViewById(R.id.textView2);

        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() == null)
        {
            finish();
            startActivity(new Intent(this,LoginActivity.class));

        }

        FirebaseUser user = firebaseAuth.getCurrentUser();
        userEmail = user.getEmail();

          int index = userEmail.indexOf("@");

          String userName = userEmail.substring(0,index);

        TextViewuser.setText("Hoşgeldin "+userName);

        btnNote.setOnClickListener(new View.OnClickListener()
                                     {
                                         @Override
                                         public void onClick(View v) {
                                             Intent intocan = new Intent(HomePageActivity.this,NoteActivity.class);
                                             startActivity(intocan);
                                         }
                                     }
        );
        btnExam.setOnClickListener(new View.OnClickListener()
                                           {
                                               @Override
                                               public void onClick(View v) {
                                                   Intent intocan = new Intent(HomePageActivity.this,ExamActivity.class);
                                                   startActivity(intocan);
                                               }
                                           }
        );
        btnHomeWork.setOnClickListener(new View.OnClickListener()
                                           {
                                               @Override
                                               public void onClick(View v) {
                                                   Intent intocan = new Intent(HomePageActivity.this,HomeWorkActivity.class);
                                                   startActivity(intocan);
                                               }
                                           }
        );
        btnExit.setOnClickListener(new View.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(View v) {
                                               if( v == btnExit)
                                               {
                                                   firebaseAuth.signOut();
                                                   finish();
                                                   startActivity(new Intent(HomePageActivity.this,LoginActivity.class));
                                               }

                                           }
                                       }
        );




    }

    @Override
    protected void onStart() {
        super.onStart();
    }


}
