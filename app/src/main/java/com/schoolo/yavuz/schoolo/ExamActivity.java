package com.schoolo.yavuz.schoolo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ExamActivity  extends AppCompatActivity {
     ImageButton BtnAddExam;
     Button btnSave;
     Button btnUpdate;
     List<Exam> exams;
     ListView listViewExams;
     DatabaseReference databaseReference;
     FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam);

        exams = new ArrayList<Exam>();
        databaseReference = FirebaseDatabase.getInstance().getReference("exams");
        BtnAddExam = (ImageButton) findViewById(R.id.BtnAddExam);
        btnUpdate = (Button) findViewById(R.id.btnUpdateForExam);
        btnSave = (Button) findViewById(R.id.btnDeleteForExam);
        listViewExams = (ListView)findViewById(R.id.listViewExams);
        firebaseAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = firebaseAuth.getCurrentUser();

        if(firebaseAuth.getCurrentUser() == null)
        {
            finish();
            startActivity(new Intent(this,LoginActivity.class));
        }
        BtnAddExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intocan = new Intent(ExamActivity.this,AddExam.class);
                    startActivity(intocan);
                }
            }
        );

        databaseReference.orderByChild("userid").equalTo(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                exams.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Exam announcement = postSnapshot.getValue(Exam.class);
                    exams.add(announcement);
                }
                ExamList announcementAdapter = new ExamList(ExamActivity.this, exams, databaseReference);
                listViewExams.setAdapter(announcementAdapter);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(ExamActivity.this,"Bağlantı hatası",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
