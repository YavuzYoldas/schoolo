package com.schoolo.yavuz.schoolo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class AddNote extends AppCompatActivity {
    Button btnSave;
    EditText edtNoteHeader;
    EditText edtNoteDetail;
    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;
    String userid;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_note);

        databaseReference = FirebaseDatabase.getInstance().getReference("notes");
        btnSave = (Button) findViewById(R.id.btnSaveNote);
        edtNoteHeader = (EditText) findViewById(R.id.editText_lesson_note);
        edtNoteDetail = (EditText) findViewById(R.id.editText_description_note);

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() == null)
        {
            finish();
            startActivity(new Intent(this,LoginActivity.class));
        }
        final FirebaseUser user = firebaseAuth.getCurrentUser();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String header = edtNoteHeader.getText().toString();
                String detail = edtNoteDetail.getText().toString();

                if(TextUtils.isEmpty(header))
                {
                    edtNoteHeader.setError("Bu alan boş bırakılamaz!");
                    return;
                }
                if(TextUtils.isEmpty(detail))
                {
                    edtNoteDetail.setError("Bu alan boş bırakılamaz!");
                    return;
                }

                    String id = databaseReference.push().getKey();
                    userid  = user.getUid();
                    Note note = new Note(id,userid,header,detail);
                    databaseReference.child(id).setValue(note);
                    Toast.makeText(AddNote.this,"Başarıyla eklendi",Toast.LENGTH_SHORT).show();

                edtNoteHeader.setText(null);
                edtNoteDetail.setText(null);
            }
        });
    }
}





