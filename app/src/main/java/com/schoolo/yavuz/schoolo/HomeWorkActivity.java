package com.schoolo.yavuz.schoolo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HomeWorkActivity extends AppCompatActivity {

    ImageButton BtnAddHomeWork;
    Button btnDelete;
    Button btnUpdate;
    List<HomeWork> homeWorks;
    ListView listViewHomeWorks;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homework);
        homeWorks = new ArrayList<HomeWork>();
        databaseReference = FirebaseDatabase.getInstance().getReference("homeworks");
        BtnAddHomeWork = (ImageButton) findViewById(R.id.BtnAddHomeWork);
        btnUpdate = (Button) findViewById(R.id.btnUpdateForHomeWork);
        btnDelete = (Button) findViewById(R.id.btnDeleteForHomeWork);
        listViewHomeWorks = (ListView)findViewById(R.id.listViewHomeWorks);

        firebaseAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = firebaseAuth.getCurrentUser();

        if(firebaseAuth.getCurrentUser() == null)
        {
            finish();
            startActivity(new Intent(this,LoginActivity.class));
        }
        BtnAddHomeWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intocan = new Intent(HomeWorkActivity.this,AddHomeWork.class);
                startActivity(intocan);
            }
        });

        databaseReference.orderByChild("userid").equalTo(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                homeWorks.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    HomeWork homeWork = postSnapshot.getValue(HomeWork.class);
                    homeWorks.add(homeWork);
                }
                HomeWorkList announcementAdapter = new HomeWorkList(HomeWorkActivity.this, homeWorks, databaseReference);
                listViewHomeWorks.setAdapter(announcementAdapter);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(HomeWorkActivity.this,"Bağlantı hatası",Toast.LENGTH_SHORT).show();
            }
        });


    }
}
