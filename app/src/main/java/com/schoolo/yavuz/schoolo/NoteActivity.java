package com.schoolo.yavuz.schoolo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class NoteActivity extends AppCompatActivity
{
    ImageButton btnAdding;
    Button btnSave;
    Button btnUpdate;
    ListView listViewNotes;
    List<Note> notes;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        notes = new ArrayList<Note>();
        databaseReference = FirebaseDatabase.getInstance().getReference("notes");
        btnAdding = (ImageButton) findViewById(R.id.BtnAddNote);
        btnUpdate = (Button) findViewById(R.id.btnUpdateForHomeNote);
        btnSave = (Button) findViewById(R.id.btnDeleteForNote);
        listViewNotes = (ListView)findViewById(R.id.listViewNotes);


        firebaseAuth = FirebaseAuth.getInstance();
        final FirebaseUser user = firebaseAuth.getCurrentUser();

        if(firebaseAuth.getCurrentUser() == null)
        {
            finish();
            startActivity(new Intent(this,LoginActivity.class));
        }

        btnAdding.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intocan = new Intent(NoteActivity.this,AddNote.class);
                startActivity(intocan);
            }
        }
        );

        databaseReference.orderByChild("userid").equalTo(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                notes.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Note note = postSnapshot.getValue(Note.class);
                    notes.add(note);
                }
                NoteList announcementAdapter = new NoteList(NoteActivity.this, notes, databaseReference);
                listViewNotes.setAdapter(announcementAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(NoteActivity.this,"Hata",Toast.LENGTH_SHORT).show();
            }
        });

    }
}
