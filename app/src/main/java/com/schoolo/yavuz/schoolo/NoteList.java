package com.schoolo.yavuz.schoolo;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import android.widget.Toast;
import java.util.List;

public class NoteList extends ArrayAdapter<Note>
{
    private Activity context;
    private List<Note> notes;
    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;
    private String userid;


    public NoteList(@NonNull Activity context, final List<Note> notes, DatabaseReference databaseReference)
    {
        super(context,R.layout.note_list, notes);
        this.context = context;
        this.notes = notes;
        this.databaseReference = databaseReference;
    }

    public View getView(int pos, View view, ViewGroup parent)
    {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.note_list,null,true);

        TextView txtName = (TextView) listViewItem.findViewById(R.id.txtNameForHomeNote);

        Button btnDelete = (Button) listViewItem.findViewById(R.id.btnDeleteForNote);

        Button btnUpdate = (Button) listViewItem.findViewById(R.id.btnUpdateForHomeNote);

        firebaseAuth = FirebaseAuth.getInstance();

        final FirebaseUser user = firebaseAuth.getCurrentUser();

        userid  = user.getUid();

        final Note note = notes.get(pos);

        txtName.setText(note.getHeader()+" Notu");

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseReference.child(note.getId()).removeValue();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showUpdateDialog(note.getId(),note.getUserid(),note.getHeader(), note.getContent());

            }
        });

        listViewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowNoteDetail(note.getHeader(), note.getContent());
            }
        });


        return listViewItem;
    }

    private void showUpdateDialog(final String noteId,String userId,String editTextHeader, String editText_content){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this.context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View dialogView = inflater.inflate(R.layout.update_dialog,null);

        dialogBuilder.setView(dialogView);

        final EditText editHeader = (EditText) dialogView.findViewById(R.id.editText1);
           editHeader.setText(editTextHeader);

        final EditText editContent = (EditText) dialogView.findViewById(R.id.editText2);
        editContent.setText(editText_content);
        final Button   UpButton = (Button) dialogView.findViewById(R.id.UpButton);



        final AlertDialog alertDialog = dialogBuilder.create();

        alertDialog.show();

        UpButton.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {

                String note_header = editHeader.getText().toString().trim();
                String edit_Content = editContent.getText().toString().trim();

                if(TextUtils.isEmpty(note_header))
                {
                    editHeader.setError("Bu alan doldurulmalıdır!");
                    return;
                }
                if(TextUtils.isEmpty(edit_Content))
                {
                    editContent.setError("Bu alan doldurulmalıdır!");
                    return;
                }
                updateNote(noteId,userid,note_header,edit_Content);
                alertDialog.dismiss();
            }
        });

    }
    private boolean updateNote(String noteId,String userId,String editTextHeader,String editText_content)
    {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("notes").child(noteId);

        Note note = new Note(noteId,userId,editTextHeader,editText_content);

        databaseReference.setValue(note);

        Toast.makeText(context,"Not Güncellendi!",Toast.LENGTH_LONG).show();

         return true;
    }
    private void ShowNoteDetail(String editTextHeader,String editText_content)
    {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this.context);

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View dialogView = layoutInflater.inflate(R.layout.note_detaile,null);

        dialogBuilder.setView(dialogView);

        final TextView editHeader = (TextView) dialogView.findViewById(R.id.textView_LessonHomeNote);

        editHeader.setText(editTextHeader);

        final TextView editContent = (TextView) dialogView.findViewById(R.id.textView_DescriptionNote);

        editContent.setText(editText_content);

        final AlertDialog alertDialog = dialogBuilder.create();

        alertDialog.show();
    }

}
