package com.schoolo.yavuz.schoolo;

public class Note {

    private String id;
    private String userid;
    private String header;
    private String content;
    public Note() {

    }

    public String getId() {
        return id;
    }

    public Note(String id,String userid,String header, String content) {
        this.id = id;
        this.userid = userid;
        this.header = header;
        this.content = content;
    }

    public void setId(String id) {
        this.id = id;

    }

    public String getHeader()
    {
        return header;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
