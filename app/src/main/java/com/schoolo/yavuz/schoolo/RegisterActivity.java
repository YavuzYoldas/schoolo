package com.schoolo.yavuz.schoolo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonRegister;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private TextView textViewSignin;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() != null)
        {
            //profil actvity here
            finish();
            startActivity(new Intent(getApplicationContext(),HomePageActivity.class));
        }



        progressDialog = new ProgressDialog(this);

        buttonRegister = (Button) findViewById(R.id.buttonRegister);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        textViewSignin = (TextView) findViewById(R.id.textViewSignin);
        buttonRegister.setOnClickListener(this);
        textViewSignin.setOnClickListener(this);
    }

    private void registerUser(){
        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            //email is empty
            Toast.makeText(this, "Email adresini giriniz.", Toast.LENGTH_SHORT ).show();
            //stopping the function exection further
            return;
        }
        if(TextUtils.isEmpty(password)) {
            //password is empty
            Toast.makeText(this, "Şifrenizi giriniz.",Toast.LENGTH_SHORT).show();
            //stopping the function execution further
            return;
        }
        //if validations are ok
        //we will first show a progress bar
        progressDialog.setMessage("Kayıt yapılıyor...");
        progressDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            //user is succesfully registered and logged in
                            //we will start the profile activity here
                            //right now lets display a toast only
                            Toast.makeText(RegisterActivity.this,"Kayıt başarılı!", Toast.LENGTH_SHORT).show();
                            finish();
                            startActivity(new Intent(getApplicationContext(),HomePageActivity.class));
                        }
                        else{

                            if (task.getException() instanceof FirebaseAuthUserCollisionException)
                            {
                                Toast.makeText(RegisterActivity.this, "Bu maile daha önceden kullanılmıştır.", Toast.LENGTH_SHORT).show();
                            }

                            String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                                    "[a-zA-Z0-9_+&*-]+)*@" +
                                    "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                                    "A-Z]{2,7}$";
                            Pattern pat = Pattern.compile(emailRegex);

                            if(pat.matcher(email).matches() == false)
                            {
                                Toast.makeText(RegisterActivity.this, "Eposta adresi yanlış girilmiştir.", Toast.LENGTH_SHORT).show();
                            }
                            if(password.length() < 6 )
                            {
                                Toast.makeText(RegisterActivity.this, "Şifre en az 6 karakter içermelidir.", Toast.LENGTH_SHORT).show();
                            }



                            Toast.makeText(RegisterActivity.this,"Kayıt başarısız! Lütfen tekrar deneyin.", Toast.LENGTH_SHORT).show();
                        }
                        progressDialog.dismiss();
                    }
                });

    }
    @Override
    public void onClick(View v) {
        if(v == buttonRegister){
            registerUser();
        }
        if(v == textViewSignin) {
            //will open login activity here
            finish();
            startActivity(new Intent(this,LoginActivity.class));
        }

    }
}
