package com.schoolo.yavuz.schoolo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Pattern;


public class AddHomeWork extends AppCompatActivity {


    Button  btnSaveNewHomework;
    EditText edtHomeWorkLesson;
    EditText edtHomeWorkDescription;
    EditText edtHomeWorkDate;
    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;
    String userid;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_homework);

        databaseReference = FirebaseDatabase.getInstance().getReference("homeworks");
        btnSaveNewHomework = (Button) findViewById(R.id.btnSaveExam_homework);
        edtHomeWorkDate = (EditText) findViewById(R.id.editText_date_homework);
        edtHomeWorkDescription = (EditText) findViewById(R.id.editText_description_homework);
        edtHomeWorkLesson = (EditText) findViewById(R.id.editText_lesson_homework);




        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() == null)
        {
            finish();
            startActivity(new Intent(this,LoginActivity.class));
        }
        final FirebaseUser user = firebaseAuth.getCurrentUser();
        btnSaveNewHomework.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String date = edtHomeWorkDate.getText().toString();
                String lesson = edtHomeWorkLesson.getText().toString();
                String description = edtHomeWorkDescription.getText().toString();



                if(TextUtils.isEmpty(date))
                {
                    edtHomeWorkDate.setError("Bu alan boş bırakılamaz!");
                    return;
                }
                if(TextUtils.isEmpty(lesson))
                {
                    edtHomeWorkLesson.setError("Bu alan boş bırakılamaz!");
                    return;
                }
                if(TextUtils.isEmpty(description))
                {
                    edtHomeWorkDescription.setError("Bu alan boş bırakılamaz!");
                    return;
                }
                String dateRegex = "\\d{2}/\\d{2}/\\d{4}";
                Pattern pat = Pattern.compile(dateRegex);
                if(pat.matcher(date).matches() == false)
                {
                    edtHomeWorkDate.setError("Formatı gg/aa/yyyy gibi olmalıdır.");
                }else
                {
                    String id = databaseReference.push().getKey();
                    userid  = user.getUid();
                    HomeWork  homeWork = new HomeWork(id,userid,lesson,date,description);
                    databaseReference.child(id).setValue(homeWork);
                    Toast.makeText(AddHomeWork.this,"Başarıyla eklendi",Toast.LENGTH_SHORT).show();

                    edtHomeWorkLesson.setText(null);
                    edtHomeWorkDescription.setText(null);
                    edtHomeWorkDate.setText(null);
                }



            }
        });

    }
}