package com.schoolo.yavuz.schoolo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Pattern;

public class AddExam extends AppCompatActivity {

    Button  btnSaveNewExam;
    EditText edtExamLesson;
    EditText edtExamDescription;
    EditText edtExamDate;
    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;
    String userid;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_exam);

        databaseReference = FirebaseDatabase.getInstance().getReference("exams");
        btnSaveNewExam = (Button) findViewById(R.id.btnSaveExam);
        edtExamDate = (EditText) findViewById(R.id.editText_date);
        edtExamDescription = (EditText) findViewById(R.id.editText_description);
        edtExamLesson = (EditText) findViewById(R.id.editText_lesson);

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() == null)
        {
            finish();
            startActivity(new Intent(this,LoginActivity.class));
        }
        final FirebaseUser user = firebaseAuth.getCurrentUser();

        btnSaveNewExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String date = edtExamDate.getText().toString();
                String lesson = edtExamLesson.getText().toString();
                String description = edtExamDescription.getText().toString();

                if(TextUtils.isEmpty(date))
                {
                    edtExamDate.setError("Bu alan doldurulmalıdır!");
                    return;
                }


                if(TextUtils.isEmpty(lesson))
                {
                    edtExamLesson.setError("Bu alan doldurulmalıdır!");
                    return;
                }
                if(TextUtils.isEmpty(description))
                {
                    edtExamDescription.setError("Bu alan doldurulmalıdır!");
                    return;
                }
                String dateRegex = "\\d{2}/\\d{2}/\\d{4}";
                Pattern pat = Pattern.compile(dateRegex);
                if(pat.matcher(date).matches() == false)
                {
                    edtExamDate.setError("Formatı gg/aa/yyyy gibi olmalıdır.");
                }else
                    {
                        String id = databaseReference.push().getKey();
                        userid  = user.getUid();
                        Exam  exam = new Exam(id,userid,lesson,date,description);
                        databaseReference.child(id).setValue(exam);
                        Toast.makeText(AddExam.this,"Başarıyla eklendi",Toast.LENGTH_SHORT).show();
                        edtExamLesson.setText(null);
                        edtExamDescription.setText(null);
                        edtExamDate.setText(null);
                    }
            }
        });



    }
}
