package com.schoolo.yavuz.schoolo;

public class HomeWork {

    private String id;
    private String userid;
    private String lesson_name;
    private String description;
    private String date;

    public  HomeWork()
    {

    }

    public HomeWork(String id,String userid,String lesson_name, String date,String description)
    {
        this.id = id;
        this.userid = userid;
        this.date = date;
        this.lesson_name = lesson_name;
        this.description = description;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLesson_name() {
        return lesson_name;
    }

    public void setLesson_name(String lesson_name) {
        this.lesson_name = lesson_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
